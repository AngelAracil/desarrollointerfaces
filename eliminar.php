<?php
	if(!isset($_GET["id"])) exit();
	$id = $_GET["id"];

	include_once "./inc/conf.php";

	$sentencia = $con->prepare("DELETE FROM city WHERE
	ID = ?;");
	$resultado = $sentencia->execute([$id]);

	if($resultado === TRUE) echo "Ciudad eliminada correctamente";
	else echo "Algo salió mal al eliminar la ciudad";
?>