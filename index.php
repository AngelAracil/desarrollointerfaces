<html>
<head>
	<meta charset="UTF-8">
	<title>Home</title>
	<link rel="stylesheet" type="text/css" href="./CSS/index.css">
	<link rel="shortcut icon" type="image/png" href="favicon.png">
</head>
<body>

	<?php 
		session_start();
		if(empty($_SESSION["user"])){
			header ("Location: logout.php");
			exit();
	}

	?>
	<h1>Practica Uno Desarrollo de Interfaces</h1>
	</br>

	<?php
	include "./list.php";
	?>
</body>
</html>