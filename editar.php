<html>
	<head>
		<meta charset="UTF-8">
		<title>Editar ciudad</title>
		<link rel="shortcut icon" type="image/png" href="favicon.png">
		<link rel="stylesheet" type="text/css" href="./CSS/form.css">
	</head>

	<body>
		
		<?php $id = $_GET["id"]; ?>

		<div class="container">

		    <form id="signup" method="post" action=<?php echo "editar.php?id=$id&primeraVez=true"?>>

		        <div class="header">
		        
		            <h3>Editar ciudad</h3>
		            
		            <p>Rellena el formulario para editar la ciudad</p>
		            
		        </div>
		        
		        <div class="sep"></div>

		        <div class="inputs">
		        
		            <input name="nombre" type="text" placeholder="Nombre de la ciudad" autofocus />

		            <input name="codigo" type="text" placeholder="Código del país" required />

		            <input name="distrito" type="text" placeholder="Distrito" />

		            <input name="poblacion" type="text" placeholder="Poblacion" />

		            <input type="submit" name="Enviar" value="Click para editar ciudad" id="submit">
		        
		        </div>

		    </form>

		</div>

	</body>
</html>
	
<?php
	
	if(!isset($_GET["id"]) ) exit();
	
	if(!empty($_GET["primeraVez"])){

	$primeraVez = $_GET["primeraVez"];

	include_once "./inc/conf.php";

	$nombre = $_POST["nombre"];
	$codigo = $_POST["codigo"];
	$distrito = $_POST["distrito"];
	$poblacion = $_POST["poblacion"];
	
	$sentencia = $con->prepare("UPDATE city SET Name = ?, CountryCode = ?, District = ?, Population = ? WHERE ID = ?;");
	$resultado = $sentencia->execute([$nombre, $codigo, $distrito,$poblacion,$id]); 

	if($resultado === TRUE) echo "Ciudad editada correctamente";
	else echo "Algo salió mal. Por favor verifica que la ciudad exista, así
	como el ID de la ciudad";
}
?>