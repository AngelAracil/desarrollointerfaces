<html>
	<head>
		<meta charset="UTF-8">
		<title>Listado</title>
		<link rel="shortcut icon" type="image/png" href="favicon.png">
		<link rel="stylesheet" type="text/css" href="./CSS/list.css">
		<link rel="stylesheet" type="text/css" href="./CSS/menuPrincipal.css">
		<style type="text/css">
			#lista {
				width: 50%;
				margin: 0 auto;
			}
			input {
				display: inline;
			}
			label {
				display: inline;
			}
		</style>
	</head>

	<body>
		<div id="MenuPrincipal">
			<ul class="menu">
			    <li><a href="./index.php">Home</a></li>
			    <li><a href="./form.php">Crear Ciudad</a></li>
			    <li><a href="./list.php">Listado de las ciudades</a></li> 
			    <li><a href="./logout.php">Cerrar Sesión</a></li> 
			</ul>
		</div>

		</br>

		<div id="Buscador">
			<form method="GET" action="list.php">
				<fieldset>
					<legend>Buscador de ciudades</legend>
					<label>Identificador:</label>
					<input type="text" name="Identificador">
					<label>Nombre:</label>
					<input type="text" name="Nombre">
					<label>Código del pais:</label>
					<input type="text" name="CodigoPais">
					<label>Distrito:</label>
					<input type="text" name="Distrito">
					<label>Población:</label>
					<input type="text" name="Poblacion">
					<input type="submit" name="Enviar" value="Enviar">

				</fieldset>
			</form>
		</div>

		</br>
		<div id="lista">

			<table border="2px">
				<tr>
					<th>Identificador</th>
					<th>Nombre</th>
					<th>Código del país</th>
					<th>Distrito</th>
					<th>Población</th>
					<th><a>Eliminar</a></th>
					<th><a>Editar</a></th>
				</tr>

				<?php

					$indicePagina = null;
					if(isset($_GET["indicePagina"])){
						$indicePagina = $_GET["indicePagina"];
					}
					include "./inc/conf.php";
					$sql = "SELECT * from city WHERE TRUE ";

					if (isset($_GET["primero"])){
						$indicePagina = 0;
					} 

					if(isset($_GET["numPaginas"])){
						$numPaginas = $_GET["numPaginas"];
					}else{
						$numPaginas = 10;
					}

					if (!isset($_GET['siguiente'])){
						$indicePagina = 0;
					}
					/*if (isset($_GET['ultimo'])){
						$obtenerTotalRegistros=$con->prepare('select count(*) from city');
						$countTotal = $obtenerTotalRegistros->execute();
						echo $countTotal . "<--------------------------";
						
					}*/

					$calculo = $indicePagina * $numPaginas;


					if(isset($_GET["Enviar"])){	

						$id = $_GET["Identificador"];
						$nombre = $_GET["Nombre"];
						$codigo= $_GET["CodigoPais"];
						$distrito = $_GET["Distrito"];
						$poblacion = $_GET["Poblacion"];

						$array = array();


						if(!empty($_GET['Identificador'])) $sql .= " and ID = :id ";
						if(!empty($_GET['Nombre'])) $sql .=  " and  Name like :nombre";
						if(!empty($_GET['CodigoPais'])) $sql .=  " and  CountryCode like :codigo";
						if(!empty($_GET['Distrito'])) $sql .=  " and  District like :distrito";
						if(!empty($_GET['Poblacion'])) $sql .=  " and  Population = :poblacion " ;

						if(!empty($_GET['Identificador'])) $array[":id"] = $id;
						if(!empty($_GET['Nombre'])) $array[":nombre"] = $nombre;
						if(!empty($_GET['CodigoPais'])) $array[":codigo"] = $codigo;
						if(!empty($_GET['Distrito'])) $array[":distrito"] = $distrito;
						if(!empty($_GET['Poblacion'])) $array[":poblacion"] = $poblacion;

						$sql .= " limit $calculo,$numPaginas";
						$stmt = $con->prepare($sql);
						$stmt->execute($array);

					}else{
						$sql .= " limit $calculo,$numPaginas";
						$stmt = $con->prepare($sql);
						$stmt->execute();

					}


					if(isset($_GET["siguiente"])) $indicePagina = $indicePagina + 1;
					if(isset($_GET["anterior"]) && $indicePagina>0) $indicePagina = $indicePagina - 1;
									
					$ciudades = $stmt->fetchAll(PDO::FETCH_OBJ);
				?>

				<?php foreach ($ciudades as $ciudad){?>
					<tr>
						<td><?php echo $ciudad->ID ?></td>
						<td><?php echo $ciudad->Name ?></td>
						<td><?php echo $ciudad->CountryCode ?></td>
						<td><?php echo $ciudad->District ?></td>
						<td><?php echo $ciudad->Population ?></td>
						<td>
							<a href="<?php echo "./eliminar.php?id=" . $ciudad->ID?>">Eliminar</a>
						</td>
						<td>
							<a href="<?php echo "./editar.php?id=" . $ciudad->ID?>">Editar</a>
						</td>
					</tr>
				<?php } ?>
			
			</table>

			<div id="Paginador">
				
				<form method="GET" action="list.php?> ">
					<input type="submit" name="primero" value="<<">
					<input type="submit" name="anterior" value="<">
					<?php echo $indicePagina ?>
					<input type="hidden" name="indicePagina" value="<?php echo $indicePagina ?>">
					<input type="submit" name="siguiente" value=">">
					<input type="submit" name="ultimo" value=">>">
					<input type="text" name="numPaginas" value="<?php echo $numPaginas ?>">
					<input type="submit" name="mostrar" name="EnviarPaginador">

				</form>

			</div>
		</div>		
	</body>
</html>

