<html>
	<head>
		<meta charset="UTF-8">
		<title>Agregar Ciudad</title>
		<link rel="shortcut icon" type="image/png" href="favicon.png">
		<link rel="stylesheet" type="text/css" href="./CSS/form.css">
		<link rel="stylesheet" type="text/css" href="./CSS/menuPrincipal.css">
	</head>
	<body>
		<div id="MenuPrincipal">
		<ul class="menu">
		    <li><a href="./index.php">Home</a></li>
		    <li><a href="./form.php">Crear Ciudad</a></li>
		    <li><a href="./list.php">Listado de las ciudades</a></li> 
		</ul>

	</div>
		<div class="container">

		    <form id="signup" method="post" action="./creadorDeCiudades.php">

		        <div class="header">
		        
		            <h3>Agregar ciudad</h3>
		            
		            <p>Rellena el formulario para agregar una ciudad</p>
		            
		        </div>
		        
		        <div class="sep"></div>

		        <div class="inputs">
		        
		            <input name="id" type="text" placeholder="Identificador" required autofocus />
		        
		            <input name="nombre" type="text" placeholder="Nombre de la ciudad" />

		            <input name="codigo" type="text" placeholder="Código del país" required />

		            <input name="distrito" type="text" placeholder="Distrito" />

		            <input name="poblacion" type="text" placeholder="Poblacion" />

		            <input type="submit" name="Enviar" value="Click para agregar ciudad" id="submit">
		        
		        </div>

		    </form>

		</div>

	</body>
</html>