<?php
	if(!isset($_POST["id"]) && !isset($_POST["nombre"])) exit();

	include_once "./inc/conf.php";
	$identificador = $_POST["id"];
	$nombre = $_POST["nombre"];
	$codigo = $_POST["codigo"];
	$distrito = $_POST["distrito"];
	$poblacion = $_POST["poblacion"];

	$sentencia = $con->prepare("INSERT INTO
	city(id, name, countrycode,district,population) VALUES (?, ?, ?, ? , ?);");
	$resultado = $sentencia->execute([$identificador,$nombre, $codigo,$distrito,$poblacion]);

	if($resultado === TRUE) {
		 echo "Ciudad insertada correctamente";
		}
	else{
		echo "Algo salió mal. Por favor vuelve a intentarlo";
		} 
?>
